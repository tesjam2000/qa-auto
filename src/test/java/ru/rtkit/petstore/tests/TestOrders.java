package ru.rtkit.petstore.tests;

import io.qameta.allure.Step;
import io.restassured.response.Response;
import org.junit.jupiter.api.DisplayName;
import ru.rtkit.petstore.helpers.Endpoints;
import ru.rtkit.petstore.models.Order;
import ru.rtkit.petstore.models.Order.Status;
import ru.rtkit.petstore.models.Pet;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Тесты для апи заказов")
public class TestOrders extends BaseTest {


    private final Pet pet = new Pet(25, "Барсик", "available");
    private final Order order = new Order(
            1,
            pet,
            3,
            "2022-07-26T10:35:13.392Z",
            Status.APPROVED,
            true
    );

    @DisplayName("создание заказа")
    @Test
    void createOrder() {
        createOrder(order);
        getOrder(order);
    }

    @Step("создаем заказ")
    void createOrder(Order newOrder) {
        apiHelper.post(Endpoints.POST_PUT_ORDER, newOrder.getJsonObject().toString(), resp200);
    }

    @Step("проверяем, что заказ существует")
    void getOrder(Order newOrder) {
        Response response = apiHelper.get(Endpoints.GET_DELETE_ORDER, resp200, newOrder.id);

        assertEquals(newOrder.id, (Integer) response.path(Order.idFieldName));
        assertInstanceOf(Boolean.class, response.path(Order.completeFieldName));
        assertNotNull(response.path(Order.shipDateFieldName));
        List<String> expectedStatuses = List.of(
                order.getStatus(Status.APPROVED),
                order.getStatus(Status.DELIVERED),
                order.getStatus(Status.PLACED)
        );
        assertTrue(expectedStatuses.contains(response.path(Order.statusFieldName)));
    }
}
