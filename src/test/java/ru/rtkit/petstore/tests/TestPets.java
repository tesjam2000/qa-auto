package ru.rtkit.petstore.tests;

import io.qameta.allure.Step;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import ru.rtkit.petstore.helpers.Endpoints;
import ru.rtkit.petstore.models.Pet;
import ru.rtkit.petstore.models.Pet.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;

@DisplayName("Тесты для апи Питомцев")
public class TestPets extends BaseTest {

    private final Pet pet = new Pet(25, "Барсик", "available");
    private final Pet petWithTag = new Pet(
            25,
            "Барсик",
            "available",
            List.of(new Tag(77, "lala"))
    );

    @Test
    @DisplayName("создание, апдейт, удаление питомца")
    void createUpdateDeletePet() {
        createPet(pet);
        Pet updatedPet = updatePet(pet);
        getPet(updatedPet);
        deletePet(updatedPet);
    }

    @Test
    @DisplayName("создание питомца с тэгами")
    void createPetWithTags() {
        createPet(petWithTag);
    }

    @ParameterizedTest(name = "создание питомца с именем {0}")
    @ValueSource(strings = {"барсик", "БАРСИК", "Barsik", "barsik", "BARSIK"})
    void differentLanguagesNames(String testName) {
        Pet testPet = new Pet(25, testName, "available");
        createAndCheckPet(testPet);
    }

    @Step("создаем питомца и проверяем, что он существует")
    void createAndCheckPet(Pet newPet) {
        createPet(newPet);
        getPet(newPet);
    }

    @Step("создаем питомца")
    void createPet(Pet newPet) {
        apiHelper.post(Endpoints.POST_PUT_PET, newPet.getJsonObject().toString(), resp200);
    }

    @Step("апдейт питомца")
    Pet updatePet(Pet createdPet) {
        Pet newPet = new Pet(createdPet.id, "Новое имя Барсика", createdPet.status);

        apiHelper.put(Endpoints.POST_PUT_PET, newPet.getJsonObject().toString(), resp200);
        return newPet;
    }

    @Step("ищем добавленного питомца")
    void getPet(Pet newPet) {
        Response getResult = apiHelper.get(Endpoints.GET_DELETE_PET, resp200, newPet.id);

        Assertions.assertEquals(newPet.name, getResult.path(Pet.nameFieldName));
        Assertions.assertEquals(newPet.id, (Integer) getResult.path(Pet.idFieldName));
        Assertions.assertEquals(newPet.status, getResult.path(Pet.statusFieldName));
    }

    @Step("удаляем питомца")
    void deletePet(Pet newPet) {
        apiHelper.delete(Endpoints.GET_DELETE_PET, resp200, newPet.id);
        apiHelper.get(Endpoints.GET_DELETE_PET, resp404, newPet.id);
    }


}
