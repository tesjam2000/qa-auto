package ru.rtkit.petstore.tests;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@DisplayName("Тесты для тренировки логирования")
public class TestLogs {

    public static Logger LOGGER = LoggerFactory.getLogger(TestLogs.class);

    @DisplayName("отображение информации в логах")
    @Test
    public void LogExample() {
        LOGGER.info("Здравствуйте не обращайте внимания");
    }

    @DisplayName("отображение ошибки в логах")
    @Test
    public void LogError() {
        LOGGER.error("Косячки в кодах");
    }
}
