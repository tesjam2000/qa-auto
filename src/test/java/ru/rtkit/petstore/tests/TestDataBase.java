package ru.rtkit.petstore.tests;

import io.qameta.allure.Step;

import org.junit.jupiter.api.Test;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TestDataBase {

    String pathToDB =
            getClass().getClassLoader().getResource("sqlite/chinook.db").getPath();
    String dbUrl = "jdbc:sqlite:" + pathToDB;

    @Step("Получение альбома из БД")
    private List<String> getAlbum() {
        String query = "SELECT * FROM albums LIMIT 1";
        List<String> albums = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(dbUrl);
             PreparedStatement ps = conn.prepareStatement(query);
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                albums.add(rs.getString("title"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return albums;
    }

    @Test
    void printAlbum() {
        System.out.println(getAlbum());
    }

    @Test
    void printPopTracks() {
        List<HashMap> popTracks = getPopTracks();
        for (HashMap track : popTracks) {
            System.out.println(track);
        }
    }

    @Step("Получение поп-треков из БД")
    private List<HashMap> getPopTracks() {
        String query = "SELECT tracks.* FROM tracks JOIN genres on tracks.GenreId = genres.GenreId " +
                "WHERE genres.Name = 'Pop'";
        List<HashMap> albums = new ArrayList<>();
        String[] fields = new String[]{"TrackId", "Name", "AlbumId", "MediaTypeId", "GenreId",
                "Composer", "Milliseconds", "Bytes", "UnitPrice"};
        try (Connection conn = DriverManager.getConnection(dbUrl);
             PreparedStatement ps = conn.prepareStatement(query);
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                HashMap<String, String> row = new HashMap<>();
                for (String field : fields) {
                    row.put(field, rs.getString(field));
                }
                albums.add(row);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return albums;
    }


}
