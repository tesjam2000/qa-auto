package ru.rtkit.petstore.models;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

public class Pet {

    public int id;
    public String name;
    public String status;
    public List<Tag> tags;

    public static String statusFieldName = "status";
    public static String nameFieldName = "name";
    public static String idFieldName = "id";
    public static String tagsFieldName = "tags";

    public Pet(int newId, String newName, String newStatus) {
        id = newId;
        name = newName;
        status = newStatus;
    }

    public Pet(int newId, String newName, String newStatus, List<Tag> newTags) {
        id = newId;
        name = newName;
        status = newStatus;
        tags = newTags;
    }

    public static class Tag {
        public int id;
        public String name;

        public static final String idFieldName = "id";
        public static final String nameFieldName = "name";

        public Tag(int newId, String newName) {
            id = newId;
            name = newName;
        }
    }

    public JSONObject getJsonObject() {
        JSONArray tagsArray = new JSONArray();
        if (tags != null) {
            for (Tag tag : tags) {
                tagsArray.put(new JSONObject().put(Tag.idFieldName, tag.id).put(Tag.nameFieldName, tag.name));
            }
        }
        return new JSONObject()
                .put(idFieldName, id)
                .put(nameFieldName, name)
                .put(statusFieldName, status)
                .put(tagsFieldName, tagsArray);
    }
}
