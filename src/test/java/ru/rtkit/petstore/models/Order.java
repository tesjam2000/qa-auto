package ru.rtkit.petstore.models;

import org.json.JSONObject;

import java.util.EnumMap;

public class Order {

    public enum Status {
        PLACED,
        APPROVED,
        DELIVERED
    }

    public EnumMap<Status, String> statuses = new EnumMap<>(Status.class);

    public int id;
    public Pet pet;
    public int quantity;
    public String shipDate;
    public Status status;
    public Boolean complete;

    public static String statusFieldName = "status";
    public static String petIdFieldName = "petId";
    public static String idFieldName = "id";
    public static String quantityFieldName = "quantity";
    public static String completeFieldName = "complete";
    public static String shipDateFieldName = "shipDate";


    public static String statusApproved = "approved";
    public static String statusDelivered = "delivered";
    public static String statusPlaced = "placed";

    public Order(int newId, Pet newPet, int newQuantity, String newShipDate, Status newStatus, Boolean newComplete) {
        statuses.put(Status.APPROVED, statusApproved);
        statuses.put(Status.DELIVERED, statusDelivered);
        statuses.put(Status.PLACED, statusPlaced);

        id = newId;
        pet = newPet;
        quantity = newQuantity;
        shipDate = newShipDate;
        status = newStatus;
        complete = newComplete;

    }

    public String getStatus(Status status) {
        return statuses.get(status);
    }

    public int getId() {
        return id;
    }
    public Pet getPet() {
        return pet;
    }
    public int getQuantity() {
        return quantity;
    }
    public String getShipDate() {
        return shipDate;
    }
    public boolean getComplete() {
        return complete;
    }
    public void setId(int newId) {
        id = newId;
    }
    public void setPet(Pet newPet) {
        pet = newPet;
    }
    public void setQuantity(int newQuantity) {
        quantity = newQuantity;
    }
    public void setShipDate(String newShipDate) {
        shipDate = newShipDate;
    }
    public void setComplete(boolean newComplete) {
        complete = newComplete;
    }

    public JSONObject getJsonObject() {
        return new JSONObject()
                .put(idFieldName, id)
                .put(petIdFieldName, pet.id)
                .put(statusFieldName, getStatus(status))
                .put(quantityFieldName, quantity)
                .put(completeFieldName, complete)
                .put(shipDateFieldName, shipDate);
    }

}
