package ru.rtkit.petstore.helpers;

import io.qameta.allure.Step;
import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import io.restassured.specification.ResponseSpecification;

import java.util.Map;

import static io.restassured.RestAssured.given;

public class ApiHelper {

    public ApiHelper(String uri) {
        RestAssured.baseURI = uri;
        RestAssured.defaultParser = Parser.JSON;
        RestAssured.filters(new AllureRestAssured());
    }


    @Step("get {endpoint}")
    public Response get(String endpoint, ResponseSpecification resp) {
        return given()
                .contentType(ContentType.JSON)
                .expect()
                .spec(resp)
                .when()
                .get(endpoint);
    }

    @Step("get {endpoint}")
    public Response get(String endpoint, ResponseSpecification resp,
                        Object... pathParams) {
        return given()
                .contentType(ContentType.JSON)
                .expect()
                .spec(resp)
                .when()
                .get(endpoint, pathParams);
    }

    @Step("get {endpoint}")
    public Response get(String endpoint, ResponseSpecification resp,
                        Map<String, String> queryParams) {
        return given()
                .params(queryParams)
                .contentType(ContentType.JSON)
                .expect()
                .spec(resp)
                .when()
                .get(endpoint);
    }

    @Step("get {endpoint}")
    public Response getWithAuth(String endpoint, ResponseSpecification
            resp) {
        return given()
                .header("api_key", "special-key")
                .contentType(ContentType.JSON)
                .expect()
                .spec(resp)
                .when()
                .get(endpoint);
    }

    @Step("post {endpoint}")
    public Response post(String endpoint, Object body,
                         ResponseSpecification resp) {
        return given()
                .contentType(ContentType.JSON)
                .body(body)
                .expect()
                .spec(resp)
                .when()
                .post(endpoint);
    }

    @Step("delete {endpoint}")
    public Response delete(String endpoint, ResponseSpecification resp,
                           Object... pathParams) {
        return given()
                .contentType(ContentType.JSON)
                .expect()
                .spec(resp)
                .when()
                .delete(endpoint, pathParams);
    }

    @Step("put {endpoint}")
    public Response put(String endpoint, Object body, ResponseSpecification
            resp, Object... pathParams) {
        return given()

                .contentType(ContentType.JSON)
                .body(body)
                .expect()
                .spec(resp)
                .when()
                .put(endpoint, pathParams);
    }
}
