package ru.rtkit.petstore.helpers;

public class Endpoints {
    public static final String POST_PUT_PET = "https://petstore.swagger.io/v2/pet";
    public static final String GET_DELETE_PET = "https://petstore.swagger.io/v2/pet/{id}";
    public static final String POST_PUT_ORDER = "https://petstore.swagger.io/v2/store/order";
    public static final String GET_DELETE_ORDER = "https://petstore.swagger.io/v2/store/order/{id}";

}
